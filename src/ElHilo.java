

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

public class ElHilo extends Thread {
	JProgressBar caballo;
	JTextArea mensajes;
	JLabel nombre;
	String nombreString;
	static int FIN = 4;
	ElHilo siguiente;
	
	
	public ElHilo(String str, JProgressBar jpb, JLabel jl) {
		
		super(str);
		nombreString=str;
		this.caballo = jpb;
		this.nombre = jl;
		this.caballo.setMinimum(0);
		this.caballo.setMaximum(FIN);		
		this.nombre.setText(str);
	}
	
	public void setSiguiente(ElHilo hilo) {
		siguiente=hilo;
	}
	
	public void run(){
		for (int i = 0; i < 5; i++) {
			//System.out.println("Posici�n " + i + ": " + getName());
			caballo.setValue(i);
			
			try {
				sleep((int) (Math.random() * 2000));
				
				//System.out.println("El caballo " + getName() + " descansa.");
			} catch (InterruptedException e) {
				//System.out.println(e);
				
			}
		}
		if(siguiente != null) {
			siguiente.start();
			
		}
		else {
			synchronized(this) {
	        	
	        	if(Ganador.ganador == null) {
	        		System.out.println("gano"+nombreString);
	        		Ganador.ganador=this;
	        	}
	        }
		}

		//System.out.println("Fin de la carrera para: " + getName());
		

	}	
	
}
