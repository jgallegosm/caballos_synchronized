



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;


import java.awt.Component;


import javax.swing.JLabel;



public class Pantalla {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla window = new Pantalla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 848, 342);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JProgressBar progressBar_c1 = new JProgressBar();
		final JProgressBar progressBar_c2 = new JProgressBar();		
		final JProgressBar progressBar_c3 = new JProgressBar();
		final JLabel lblNewLabel_c1 = new JLabel("New label");		
		final JLabel lblNewLabel_c2 = new JLabel("New label");		
		final JLabel lblNewLabel_c3 = new JLabel("New label");
	        
		JButton btnEmpezar = new JButton("Empezar carrera");
		
		JLabel posta_1 = new JLabel("New label");
		
		JLabel posta_3 = new JLabel("New label");
		
		JLabel posta_2 = new JLabel("New label");
		
		JProgressBar progressBar_posta_1 = new JProgressBar();
		
		JProgressBar progressBar_posta_2 = new JProgressBar();
		
		JProgressBar progressBar_posta_3 = new JProgressBar();
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_c3)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(lblNewLabel_c1)
							.addComponent(lblNewLabel_c2)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(btnEmpezar, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(progressBar_c1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(progressBar_c2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(progressBar_c3, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE))
					.addGap(49)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(posta_1)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(progressBar_posta_1, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(posta_3)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(progressBar_posta_3, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(posta_2)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(progressBar_posta_2, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE)))
					.addGap(187))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_c1)
								.addComponent(progressBar_c1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_c2)
								.addComponent(progressBar_c2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(posta_1)
								.addComponent(progressBar_posta_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(progressBar_posta_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(posta_2))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_c3)
								.addComponent(progressBar_c3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnEmpezar))
						.addComponent(posta_3)
						.addComponent(progressBar_posta_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(475, Short.MAX_VALUE))
		);
		groupLayout.linkSize(SwingConstants.VERTICAL, new Component[] {progressBar_c1, progressBar_c2, progressBar_c3});
		frame.getContentPane().setLayout(groupLayout);
		
		
		btnEmpezar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ElHilo hilo1 = new ElHilo("Tornado", progressBar_c1, lblNewLabel_c1);
				ElHilo hilo2 = new ElHilo("Pegaso", progressBar_c2, lblNewLabel_c2);
				ElHilo hilo3 = new ElHilo("Socrates", progressBar_c3, lblNewLabel_c3);
				
				ElHilo hilo11 = new ElHilo("Tornado2", progressBar_posta_1, posta_1);
				ElHilo hilo21 = new ElHilo("Pegaso2", progressBar_posta_2, posta_2);
				ElHilo hilo31 = new ElHilo("Socrates2", progressBar_posta_3, posta_3);
				
				hilo1.setSiguiente(hilo11);
				hilo2.setSiguiente(hilo21);
				hilo3.setSiguiente(hilo31);
				hilo1.start();
				hilo2.start();
				hilo3.start();
				
			}
		});	
		
	}	
}
